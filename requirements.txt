Django==2.1.4
Pillow==5.4.0
pytz==2018.7
sorl-thumbnail==12.5.0
psycopg2-binary==2.7.*